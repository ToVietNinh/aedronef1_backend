package define

type UserStatus string

const (
	INACTIVE    = "inactive"
	ACTIVE      = "active"
	FIRST_LOGIN = "first_login"

	TYPE_USER = "user"
)
